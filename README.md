# WES

Petit package qui permet l'installation rapide des differents composants utiles à la réalisation des tests fonctionnels.

### Packagist

This package can be found on [Packagist](http://packagist.org) and installed with [Composer](https://getcomposer.org/).

Commandes :
```
php composer.phar require nemrod/wes
php vendor/nemrod/wes/init.php
```