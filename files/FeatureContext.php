<?php

use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterStepScope;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends Behat\MinkExtension\Context\MinkContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        
    }

    /**
     * @BeforeStep
     */
    public function resizeWindowByDefault()
    {
        $this->getSession()->resizeWindow(1440,900, 'current');
    }

    /**
     * @AfterStep
     */
    /*public function takeScreenshotAfterStepFailed(AfterStepScope $scope) {
        if (99 === $scope->getTestResult()->getResultCode()) {
            $html = $this->getSession()->getDriver()->getContent();
            file_put_contents('tmp/'.date("H_i_s").'_'.str_replace([' ', '"', '/'], '_', $scope->getStep()->getText()).'_.html', $html);
        }
    }*/

}
