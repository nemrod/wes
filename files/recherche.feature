# language: fr

Fonctionnalité: Allez sur le site de WES

Scénario: Je vais sur le site
  Lorsque je vais sur "https://www.webetsolutions.com/"
  Alors je devrais voir "Vendez aussi bien sur le web qu’en magasin avec une solution e-commerce multicanal, et gérez tout d’une seule interface !"
